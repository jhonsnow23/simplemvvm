package com.jonel.simple.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.jonel.simple.storage.PrefManager

class SplashScreenViewModelFactory(val prefManager: PrefManager) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return SplashScreenViewModel(prefManager) as T
    }

}