package com.jonel.simple.view

import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.jonel.simple.R
import com.jonel.simple.storage.PrefManager
import com.jonel.simple.viewmodel.SplashScreenViewModel
import com.jonel.simple.viewmodel.SplashScreenViewModelFactory

class SplashScreenActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen_layout)
        val prefManager = PrefManager(this)
        val viewFactory = SplashScreenViewModelFactory(prefManager)
        val viewModel = ViewModelProviders.of(this, viewFactory).get(SplashScreenViewModel::class.java)
        if (viewModel.isLogged()) {
            startActivity(Intent(this@SplashScreenActivity, LoginActivity::class.java))
            finish()
        } else {
            //add activity or some view here if it
        }
    }
}