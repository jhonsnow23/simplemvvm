package com.jonel.simple.viewmodel

import android.arch.lifecycle.ViewModel
import com.jonel.simple.storage.PrefManager

class SplashScreenViewModel(val prefManager: PrefManager) : ViewModel() {

    fun isFirstTimeLaunch(): Boolean {
        return prefManager.isFirstTimeLaunch()
    }

    fun setFirstTimeLaunch(firstTime: Boolean) {
        prefManager.setFirstTimeLaunch(firstTime)
    }

    fun isLogged(): Boolean {
        return prefManager.isLoggedIn()
    }

    fun setLoggedin(loggedIn: Boolean) {
        prefManager.setLoggedIn(loggedIn)
    }
}