package com.jonel.simple.storage

import android.content.Context
import android.content.SharedPreferences

class PrefManager(private val context: Context) {

    val IS_FIRST_TIME_LAUNCH = "isFirstTimeLaunch"
    val LOGGED_IN = "logged_in"
    private var sharedPreferences = context.getSharedPreferences("pref_manager", Context.MODE_PRIVATE)
    private var editor: SharedPreferences.Editor

    init {
        editor = sharedPreferences.edit()
        editor.apply()
    }

    fun setFirstTimeLaunch(isFirstTime: Boolean) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime)
        editor.commit()
    }

    fun isFirstTimeLaunch() = sharedPreferences.getBoolean(IS_FIRST_TIME_LAUNCH, true)

    fun setLoggedIn(isLoggedIn: Boolean) {
        editor.putBoolean(LOGGED_IN, isLoggedIn)
        editor.commit()
    }

    fun isLoggedIn() = sharedPreferences.getBoolean(LOGGED_IN, true)

}
