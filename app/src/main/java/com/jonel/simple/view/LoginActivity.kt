package com.jonel.simple.view

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.jonel.simple.R
import com.jonel.simple.viewmodel.LoginViewModel
import kotlinx.android.synthetic.main.loggin_layout.*

class LoginActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loggin_layout)
        val viewModel = ViewModelProviders.of(this).get(LoginViewModel::class.java)

        button.setOnClickListener {
            viewModel.pressLogin(editText.text.toString(), editText2.text.toString())
        }

        viewModel.userLiveData.observe(this, Observer {
            if (it == 200) {
                Toast.makeText(this, "Email and Password are correct!", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Wrong email and Password!!", Toast.LENGTH_SHORT).show()
            }
        })
    }

}